<?php

/**
 * @file
 * AAT S3 Utils Customisations
 *
 * Provide a module for additional S3 utilities.
 *
 * @author Stephen Blinkhorne
 * @package aat_other
 *
 */

class AatS3Utils {

  // The following list is taken from CannedAcl class.
  const PRIVATE_ACCESS = 'private';
  const PUBLIC_READ = 'public-read';
  const PUBLIC_READ_WRITE = 'public-read-write';
  const AUTHENTICATED_READ = 'authenticated-read';
  const BUCKET_OWNER_READ = 'bucket-owner-read';
  const BUCKET_OWNER_FULL_CONTROL = 'bucket-owner-full-control';

  protected $s3;
  private $config;

  public function __construct() {
    $this->s3 = $this->aat_s3_utils_get_connector();
  }

  /**
   * Get the S3 connector.
   *
   * We first try to get the connector information from the s3fs module.
   *
   * @return \Aws\S3\S3Client|bool|null
   */
  private function aat_s3_utils_get_connector() {
    $this->config = [];
    $s3 = NULL;

    // Make sure the s3fs module is enabled and function exists.
    if (module_exists('s3fs')) {
      $this->config = _s3fs_get_config();

      try {
        $s3 = _s3fs_get_amazons3_client($this->config);
      }
      catch (Exception $e) {
        watchdog('AatS3Utils', $e->getMessage());
        return FALSE;
      }
    }

    return $s3;
  }

  /**
   * Set the metatags on the s3 object.
   *
   * Currently not used but was developed during testing and may be handy for future use.
   *
   * @param string $file
   *   S3 path of the file to change.
   * @param array $metadata
   *   List of metadata to be changed,
   */
  public function aat_s3_utils_update_metatags($file, $metadata) {
    // Check to make sure we have the file and meta tags to update.
    if (empty($metadata) || empty($file) || empty($this->config)) {
      return;
    }

    // To change the metadata we need to copy the object.
    $this->copyObject($file, $metadata);
  }

  /**
   * Update the robots meta tag on the file specified.
   *
   * @param string $file
   *   The file and and path to be updated.
   * @param bool $index
   *   Set to true if we need to add the robots tag to the object. Otherwise we set it to false.
   */
  public function aat_s3_utils_update_robots_tag($file, $index = TRUE) {
    // Check to make sure we have the file and S3 config settings to update.
    if ( empty($file) || empty($this->config)) {
      return;
    }

    $metadata = ['X-Robots-Tag' => 'noindex'];

    if ($index) {
      $metadata = ['X-Robots-Tag' => 'all'];
    }

    // Need to read the current settings before copying the file.
    // Need to identify the bucket.
    $this->copyObject($file, $metadata);
  }

  /**
   * Get the s3 client.
   *
   * @return \Aws\S3\S3Client|bool|null
   */
  public function getClient() {
    return $this->s3;
  }

  /**
   * Copy the S3 object and update the meta tags.
   *
   * @param string $file
   *   The file name and path to be copied.
   * @param array $metadata
   *   The metadata to be changed.
   */
  private function copyObject($file, $metadata) {
    // Check the object exists.
    if ($this->s3->doesObjectExist($this->config['bucket'], $file) == FALSE) {
      return;
    }

    // Get the current metadata on the file.
    $current_headers = $this->getHeaders($file);
    $acl = $this->getAcl($file);
    // Add the bucket to the source file.
    $source = $this->config['bucket'] . '/' . $file;

    $this->s3->copyObject(array(
      'Bucket' => $this->config['bucket'],
      'CopySource' => $source,
      'Key' => $file,
      'Metadata' => $metadata,
      'ContentType' => $current_headers->get('ContentType') ,
      'ACL' => $acl,
      'MetadataDirective' => 'REPLACE',
    ));
  }

  /**
   * Retrieves metadata from an object without returning the object itself.
   *
   * @param string $file
   *   File name and path where to get metadata from.
   *
   * @return \Aws\Result|\Guzzle\Service\Resource\Model
   */
  private function getHeaders($file) {
    // Need to get the current metadata so when we copy the object we can keep it.
    $result_head = $this->s3->headObject(
      [
        'Bucket' => $this->config['bucket'],
        'Key' => $file,
      ]
    );

    return $result_head;
  }

  /**
   * Get the current public ACL setting for the specified file.
   *
   * @param string $file
   * File name and path.
   *
   * @return string
   *
   */
  private function getAcl($file) {

    $object_acl = $this->s3->getObjectAcl(
      [
        'Bucket' => $this->config['bucket'],
        'Key' => $file,
      ]
    );

    // Set the default ACL setting to block access.
    $acl = 'private';

    $grants = $object_acl->get('Grants');
    foreach ($grants as $grant) {
      // We are only interested in public settings.
      if (!empty($grant['Grantee']['URI']) && strpos($grant['Grantee']['URI'],'AllUsers') !== FALSE) {
        switch ($grant['Permission']) {
          case 'READ':
            // Just check to make sure the acl hasn't already been set other than the default setting.
            if ($acl !== self::PUBLIC_READ_WRITE) {
              $acl = self::PUBLIC_READ;
            }
            break;
          case 'WRITE':
            $acl = self::PUBLIC_READ_WRITE;
            break;
        }
      }
    }
    return $acl;
  }

}
